﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marazzo_Responsive.Domain.Enums
{
    public enum EntityStatus
    {
        Active =1,
        Inactive =2,
    }
}
