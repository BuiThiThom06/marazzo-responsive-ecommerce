﻿
using Marazzo_Responsive.Application.Services;
using Marazzo_Responsive.Domain.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marazzo_Responsive.Application
{
    public static class ServiceCollectionExtension 
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IProductService, ProductService>();
        }
    }
}
