﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marazzo_Responsive.Domain.Abtractions
{
    public interface IUnitOfWork
    {
        Task SaveChangAsync();
        Task<IDbContextTransaction> BeginTransactionAsync();
    }
}
