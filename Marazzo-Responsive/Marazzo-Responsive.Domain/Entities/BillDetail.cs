﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marazzo_Responsive.Domain.Abtractions
{
	[Table("BillDetails")]
	public class BillDetail : BaseEntity<Guid>
	{
		[Column(TypeName = "nvarchar(1000)")]
		public string ProductName { get; set; }
		public int Quantity { get; set; }

		[Column(TypeName = "decimal(18,2)")]
		public decimal UnitPrice { get; set; }

		public Guid BillId { get; set; }

		[ForeignKey(nameof(BillId))]
		public Bill Bill { get; set; }

	}
}
